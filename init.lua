package.loaded.freedesktop = nil

local freedesktop =
{
    desktop  = require("freedesktop.desktop"),
    utils    = require("freedesktop.utils"),
    dirs     = require("freedesktop.dirs"),
    menu     = require("freedesktop.menu")
}

return freedesktop
